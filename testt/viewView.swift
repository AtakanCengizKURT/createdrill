//
//  viewView.swift
//  testt
//
//  Created by Atakan Cengiz KURT on 17.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class viewView: UIView, UIGestureRecognizerDelegate {
    let circleWidth = CGFloat(25)
    let circleHeight = CGFloat(25)
    let circleView = UIView()
    
    var flag = true
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        circleView.isUserInteractionEnabled = true
        for subview in self.subviews {
            if gestureRecognizer.view == subview{
                subview.removeFromSuperview()
            }
        }
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let circleCenter = touch.location(in: self)
            let circleView = CircleView(frame: CGRect(x: circleCenter.x - circleWidth/2, y: circleCenter.y - circleWidth/2, width: circleWidth, height: circleHeight))
            for subview in self.subviews{
                if touch.view == subview{
                    subview.removeFromSuperview()
                }else{
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap(_:)))
                    tap.delegate = self
              
                    circleView.addGestureRecognizer(tap)
                 
                    self.addSubview(circleView)
                }
            }
        }
    }

}

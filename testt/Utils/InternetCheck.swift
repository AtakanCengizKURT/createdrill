//  Created by Atakan Cengiz KURT

import Foundation
import UIKit
import SystemConfiguration

/*
//internet kontrolü
if isInternetAvailable() == false
{
    let alert = UIAlertController(title: "İNTERNET HATASI", message: "İnternet bağlantınız yok!", preferredStyle: UIAlertControllerStyle.alert)
    
    
    let btnCancel = UIAlertAction(title: "Tamam", style: UIAlertActionStyle.cancel) { (ACTION) in
        
    }
    
    alert.addAction(btnCancel)
    if let keyWindow = UIApplication.shared.keyWindow {
        
        if !(keyWindow.rootViewController!.presentedViewController is UIAlertController) {
            keyWindow.rootViewController!.present(alert, animated: true)
        }
        
    }
    
}
//internet kontrolü
 */

//İnternet kontrolü
func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    return (isReachable && !needsConnection)
}
//internet kontrolü

//alert
func alertController(title: String, message: String)
{
   
    let alert = UIAlertController(title: NSLocalizedString(title, comment: ""), message: NSLocalizedString(message, comment: ""), preferredStyle: UIAlertController.Style.alert)
    
    
    let btnCancel = UIAlertAction(title: NSLocalizedString("Tamam", comment: ""), style: UIAlertAction.Style.cancel) { (ACTION) in
        
    }
    alert.addAction(btnCancel)
    if let keyWindow = UIApplication.shared.keyWindow {
        
        if !(keyWindow.rootViewController!.presentedViewController is UIAlertController) {
            keyWindow.rootViewController!.present(alert, animated: true)
        }
        
    }
}
//alert
//////////////

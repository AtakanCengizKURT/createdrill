//
//  ACKImage.swift
//  testt
//
//  Created by Atakan Cengiz KURT on 17.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
    
 
}





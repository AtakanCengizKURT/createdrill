//
//  alert.swift
//  testt
//
//  Created by Atakan Cengiz KURT on 17.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import Foundation
import UIKit


func alert(title: String, message: String){
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)

    let btnCancel = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: UIAlertAction.Style.cancel) { (ACTION) in
        
    }
    alert.addAction(btnCancel)
    
    if let keyWindow = UIApplication.shared.keyWindow {
        
        if !(keyWindow.rootViewController!.presentedViewController is UIAlertController) {
            keyWindow.rootViewController!.present(alert, animated: true)
        }
        
    }
}

//
//  ViewController.swift
//  testt
//
//  Created by Atakan Cengiz KURT on 17.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDifficulty: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var btnClosePicker: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var cnsB_pickerView: NSLayoutConstraint!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgDate: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var cnsB_picker: NSLayoutConstraint!
    var yellowCircleArray = [CGFloat:CGFloat]()
   
    
    
    struct Drill: Codable {
        var name: String
        var type: String
        var difficulty: Int
        var time: Int
        var image: String
    }
    
    
    struct Response:Codable {
        var success: String
    }
    
    var models : Response!
    
    enum type:String, CaseIterable {
        case FreeThrow = "Free Throw"
        case TwoPoints = "Two Points"
        case ThreePoints = "Three Points"
        case FreeShot = "Free Shot"
    }
    
    
    @IBOutlet weak var fieldView: UIView!
    let circleWidth = CGFloat(25)
    let circleHeight = CGFloat(25)
    let circleView = UIView()
    var flag = true
    
    var fieldvieww = UIView()
    private var selectedDateAsTimeStamp: Double = 0.0
    private var timeArray: [Int] = [Int]()
    private var difficultyArray: [Int] = [Int]()
    private var typeArray: [String] = [String]()
    
    var imageData:Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtName.delegate = self
        
        self.btnClosePicker.alpha = 0
        let caseList = type.allCases.map({$0.rawValue})
        typeArray = caseList

        for i in 18..<66 {
            timeArray.append(i)
        }
        for d in 1...5{
           difficultyArray.append(d)
        }
    }
    

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.txtName.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func action_date(_ sender: Any) {
        self.cnsB_pickerView.constant = 0
        
        UIView.animate(withDuration: 0.3) {
            () -> Void in
            self.view.layoutIfNeeded()
            self.btnClosePicker.alpha = 1
        }
    }
    
    @IBAction func action_picker_done(_ sender: Any) {
        self.cnsB_pickerView.constant = -300
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.btnClosePicker.alpha = 0
            self.view.layoutIfNeeded()
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM YYYY"
        let dateString = dateFormatter.string(from: datePicker.date)
        
        self.lblDate.text = dateString
        self.selectedDateAsTimeStamp = datePicker.date.timeIntervalSince1970
    }
    @IBAction func action_close_picker(_ sender: Any) {
        self.cnsB_pickerView.constant = -300
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.btnClosePicker.alpha = 0
            self.view.layoutIfNeeded()
        }
    }
    
    
     func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    
    @IBAction func action_submit(_ sender: Any) {
        
        for subview in fieldView.subviews{
            
            if subview.layer.frame.height == 25{
                self.yellowCircleArray[subview.layer.frame.origin.x] = subview.layer.frame.origin.y
            }
        }
        print(yellowCircleArray)
        
        
        for i in yellowCircleArray{
            let fieldVieww = YellowCircleView(frame: CGRect(x: i.key , y: i.value , width: circleWidth, height: circleHeight))
            self.fieldView.addSubview(fieldVieww)
        }
        
        //view to image
        UIGraphicsBeginImageContext(self.fieldView.bounds.size)
        self.fieldView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenShot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        print(convertImageToBase64(image: screenShot!).count)
        
        
        
        
        
        
        if let imageData = screenShot!.jpeg(.low) {
        self.imageData = imageData
            image.image = UIImage(data: imageData)
        }
        
        guard let convertImage: String = convertImageToBase64(image: UIImage(data: imageData!)!) else {
        
            return
        }
        
        guard let name: String = self.txtName.text, name.count > 0 else {
         alert(title: "Attention", message: "Please enter name")
            return
        }
        
        guard let type: String = self.lblType.text, type.count > 0, type != "Select Type" else {
            alert(title: "Attention", message: "Please select type")
            return
        }

        guard let difficulty: Int = Int(self.lblDifficulty.text!) else{
             alert(title: "Attention", message: "Please select difficulty")
            return
        }

            guard let time: Int = Int(self.lblTime.text!) else{
                alert(title: "Attention", message: "Please select time")
                return
        }
        ActivityIndicator().customActivityIndicatory(self.view, startAnimate: true)
        let drill = Drill(name: name, type: type, difficulty: difficulty, time: time, image: convertImage)
        do {
            let jsonEncoder = JSONEncoder()
            let encodeDrill = try jsonEncoder.encode(drill)
            
            let url = URL(string: "http://ec2-18-188-69-79.us-east-2.compute.amazonaws.com:3000/createDrill")
            var request = URLRequest(url: url!)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            request.httpBody = encodeDrill
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data, error == nil else{
                    print("error= \(error)")
                    return
                }
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200{
                    ActivityIndicator().customActivityIndicatory(self.view, startAnimate: false)
                    print(httpStatus.statusCode)
                    print(response)
                }
                let responseString = String(data: data, encoding: .utf8)
                
                
                do{
                    
                    let jsonResponse = try JSONSerialization.jsonObject(with:
                        data, options: [])
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Response.self, from:
                        data) //Decode JSON Response Data
                    
                    self.models = model
                    print(self.models.success)
                    ActivityIndicator().customActivityIndicatory(self.view, startAnimate: false)
                    if self.models.success == "ok"{
                        
                        
                        if let createdController: CreatedViewController = PDVAPI_PageManager.getStoryboardWithIdentifierWithStoryboardId("Main", "CreatedViewController") as? CreatedViewController {
                            createdController.modalTransitionStyle = .crossDissolve
                            createdController.modalPresentationStyle = .overCurrentContext
                            createdController.home = ({
                                self.navigationController?.popToRootViewController(animated: true)
                            })
                            DispatchQueue.main.async {
                                self.present(createdController, animated: true)
                            }
                            
                        }
                        
                    }
                    
                    
                } catch let parsingError {
                    ActivityIndicator().customActivityIndicatory(self.view, startAnimate: false)
                    print("Error", parsingError)
                }
                
            }
            task.resume()
            
        } catch let error {
            ActivityIndicator().customActivityIndicatory(self.view, startAnimate: false)
            print(error)
        }
        
        
        
        
       
        
        
        
        
        
    }
    
    
    @IBAction func action_cancel(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    
 
    @IBAction func action_select_type(_ sender: Any) {
        self.cnsB_picker.constant = 0
        self.picker.tag = 1
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.view.layoutIfNeeded()
        }
        self.picker.reloadAllComponents()
        self.picker.selectRow(0, inComponent: 0, animated: false)
    }
    
    @IBAction func action_select_diffculty(_ sender: Any) {
        self.cnsB_picker.constant = 0
        self.picker.tag = 2
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.view.layoutIfNeeded()
        }
        self.picker.reloadAllComponents()
        self.picker.selectRow(0, inComponent: 0, animated: false)
    }
    
    @IBAction func action_select_time(_ sender: Any) {
        self.cnsB_picker.constant = 0
        self.picker.tag = 3
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.view.layoutIfNeeded()
        }
        self.picker.reloadAllComponents()
        self.picker.selectRow(0, inComponent: 0, animated: false)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if picker.tag == 1 {
            
            return self.typeArray.count
            
        }else if picker.tag == 2 {
            
            return self.difficultyArray.count
            
        }else{
            
            return self.timeArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if picker.tag == 1 {
            return self.typeArray[row]
        } else if picker.tag == 2 {
            return String(self.difficultyArray[row])
        } else {
            return String(self.timeArray[row])
        }
    }
    
    @IBAction func action_singlePicker_done(_ sender: Any) {
        self.cnsB_picker.constant = -300
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.view.layoutIfNeeded()
            if self.picker.tag == 1 {
                self.lblType.text = self.typeArray[self.picker.selectedRow(inComponent: 0)]
            }else if self.picker.tag == 2 {
                self.lblDifficulty.text = "\(self.difficultyArray[self.picker.selectedRow(inComponent: 0)])"
            }else{
                self.lblTime.text = "\(self.timeArray[self.picker.selectedRow(inComponent: 0)])"
            }
            
            
        }
        

    }
    
    
    
}


//
//  MainViewController.swift
//  testt
//
//  Created by Atakan Cengiz KURT on 17.11.2018.
//  Copyright © 2018 AtakanCengizKURT. All rights reserved.
//

import UIKit
import Crashlytics

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func action_crashlytics(_ sender: Any) {
        Crashlytics.sharedInstance().crash()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  CustomDrillListViewController.swift
//  testt
//
//  Created by Atakan Cengiz KURT on 17.11.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class CustomDrillListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    struct Drills: Codable {
        var difficulty : Int
        var image : String
        var name : String
        var time :Int
        var type : String
    }
    var models = [Drills]()
    
    
    @IBOutlet weak var tblList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityIndicator().customActivityIndicatory(self.view, startAnimate: true)
        guard let url = URL(string: "http://ec2-18-188-69-79.us-east-2.compute.amazonaws.com:3000/drills") else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    return }
            do{
             
                let jsonResponse = try JSONSerialization.jsonObject(with:
                    dataResponse, options: [])
                
                let decoder = JSONDecoder()
                let model = try decoder.decode([Drills].self, from:
                    dataResponse) //Decode JSON Response Data
                
                self.models = model
//                print("name=\(self.models)")

            } catch let parsingError {
                print("Error", parsingError)
            }
            
            DispatchQueue.main.async {
                self.tblList.reloadData()
                ActivityIndicator().customActivityIndicatory(self.view, startAnimate: false)
            }
            
            
        }
        task.resume()
       
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: CustomDrillListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! CustomDrillListTableViewCell{
            
            
            cell.lblName.text = models[indexPath.row].name
            cell.lblTime.text = String(models[indexPath.row].time)
            cell.lblType.text = models[indexPath.row].type
            cell.lblDifficulty.text = String(models[indexPath.row].difficulty)
            cell.img.image = base64Convert(base64String: models[indexPath.row].image)
            
           return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 126
    }
    
    
    
    
    func base64Convert(base64String: String?) -> UIImage{
        
        if (base64String?.isEmpty)! {
            return #imageLiteral(resourceName: "picture")
        }else {
            if let base64String = base64String{
                if let dataDecoded : Data = Data(base64Encoded: base64String, options: .ignoreUnknownCharacters){
                    if let decodedimage = UIImage(data: dataDecoded){
                        return decodedimage
                    }else{
                        return UIImage(named: "picture")!
                    }
                }else{
                    return UIImage(named: "picture")!
                }
            }else{
                return UIImage(named: "picture")!
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
